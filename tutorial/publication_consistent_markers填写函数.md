# 新的cell_type填写教程

## 填写时复制以下代码: 

- 第一个cell_type前请运行

`df_cell_type = pd.DataFrame()`

- 对每个cell_type(subtype)都要运行此段代码

```python
cell_type = {
    'name':'',
    'cell_ontology_name':'',
    'subtype_name':''
}
marker1 = [
    [],
    1,
    ''
]
markers = []
df_cell_type = my_builder.add_data_to_df(markers=markers,cell_type=cell_type,df_cell_type=df_cell_type)
```

- 最后一个cell_type后请运行

`cell_types = my_builder.df_to_cell_types(df_cell_type)`

## 填写说明

### cell_type:

- 类型: 字典

- name: 非subtype直接填写名称, 如果是subtype, 填写父级celltype的名称

- cell_ontology_name: 填写cell_type对应的ontology_name. 当正在填写的是subtype时, ontlogy_name填写subtype的ontology

- subtype_name: 仅当填写subtype时填写

- 举例: cell_type_1存在subtype: cell_type2, 则cell_type_1填写为:

    `cell_type = {'name': 'cell_type_1', 'cell_ontology_name': 'co_name_1', 'subtype_name': ''}`

    而cell_type2填写为: 

    `cell_type = {'name': 'cell_type_1', 'cell_ontology_name': 'co_name_2', 'subtype_name':' cell_type_2'}`

### marker

每个cell_type都会有marker, 数量至少一个. 

- marker类型: list

    - 每个marker里有三个元素: `'genes','weight','comment'`, 数据类型分别为list, int(or float), string. 
        - genes列表中的元素是字符串的基因名称, positiveMarker后加符号`+`,negativeMarker后加符号`-`. 
        - weight是每个marker的权重. 对单个cell_type(subtype)来说, 所有的marker的权重和为1. 
        - comment: 按照文章给的填写, 不填时第三项填空字符串. 可以参考https://github.com/klarman-cell-observatory/pegasus/tree/master/pegasus/annotate_cluster

- 举例: T cell 存在两个marker. 

    ```python
    cell_type = {
        'name':'T cell',
        'cell_ontology_name': 'T cell',
        'subtype_name':''
    }
    
    marker1 = [
        ['CD3D+','CD3E+','CD3G+'],
        0.7,
        'positive markers'
    ]
    
    marker2 = [
        ['gene4-','gene5-'],
        0.3,
        'negative markers'
    ]
    ```
    
    



## 解释

整个过程就是在操作一个DataFrame(`df_cell_types`), 每一步都是将一个cell_type的信息追加在一开始的空dataframe(`df_cell_type`)里. 最后用脚本处理将按格式填好的dataframe转化为json可读的格式(`cell_types = my_builder.df_to_cell_types(df_cell_type)`)

之后将整合的cell_types与others信息一起转化成json格式存储. 





## 示例

示例cell_type来源: https://www.jianshu.com/p/de434e43013b

```python
df_cell_type = pd.DataFrame()
```

```python
cell_type = {
    'name':'B cells',
    'cell_ontology_name':'B cell',
    'subtype_name':''
}
marker1 = [
    ['CD19+','MS4A1+','CD79A+'],
    1,
    'positive markers'
]
markers = [marker1]
df_cell_type = my_builder.add_data_to_df(markers=markers,cell_type=cell_type,df_cell_type=df_cell_type)
```

![](images/pc_tutorial-1.png)

```python
cell_type = {
    'name':'T cells',
    'cell_ontology_name':'T cell',
    'subtype_name':''
}
marker1 = [
    ['CD3D+','CD3E+','CD3G+'],
    0.75,
    'positive markers'
]
marker2 = [
    ['gene4-','gene5-'],
    0.25,
    'negative markers'
]
markers = [marker1, marker2]
df_cell_type = my_builder.add_data_to_df(markers=markers,cell_type=cell_type,df_cell_type=df_cell_type)
```

![](images/pc_tutorial-2.png)

```python
cell_type = {
    'name':'T cells',
    'cell_ontology_name':'CD4-positive helper T cell',
    'subtype_name':'CD4 T cells'
}
marker1 = [
    ['CD4+','FOXP3+','IL2RA+','IL7R+'],
    0.6,
    'strong positive markers'
]
marker2 = [
    ['gene1+','gene2+','gene3+'],
    0.2,
    'positive markers'
]
marker3 = [
    ['gene4-','gene5-'],
    0.2,
    'negative markers'
]
markers = [marker1,marker2,marker3]
df_cell_type = my_builder.add_data_to_df(markers=markers,cell_type=cell_type,df_cell_type=df_cell_type)
```

![](images/pc_tutorial-3.png)

```python
cell_type = {
    'name':'T cells',
    'cell_ontology_name':'CD8-positive, alpha-beta T cell',
    'subtype_name':'CD8 T cells'
}
marker1 = [
    ['CD8A+','CD8B+'],
    1,
    'positive markers'
]
markers = [marker1]
df_cell_type = my_builder.add_data_to_df(markers=markers,cell_type=cell_type,df_cell_type=df_cell_type)
```

![](images/pc_tutorial-4.png)

```python
cell_types = my_builder.df_to_cell_types(df_cell_type)
```

![](images/pc_tutorial-5.png)
